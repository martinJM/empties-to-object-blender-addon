bl_info = {
    "name": "Empties to Object",
    "author": "MartinJM",
    "version": (0, 0, 3),
    "blender": (2, 80, 0),
    "description": "Create objects from selected empties.",
    "wiki_url": "https://gitlab.com/martinJM/empties-to-object-blender-addon",
    "tracker_url": "https://gitlab.com/martinJM/empties-to-object-blender-addon",
    "category": "Object",
}

if "bpy" in locals():
    import imp
    imp.reload(SelectionHelpers)
    imp.reload(CreationHelpers)
    imp.reload(SelectedEmptiesToPlane)
    imp.reload(SelectedEmptiesToVertices)
else:
    from . import (
        SelectionHelpers,
        CreationHelpers,
        SelectedEmptiesToPlane,
        SelectedEmptiesToVertices
    )

import bpy

class ETO_MT_menu(bpy.types.Menu):
    bl_idname = "eto.menu"
    bl_label = "Empties to Object"

    def draw(self, context):
        self.layout.operator(SelectedEmptiesToPlane.ETO_tracked_plane.bl_idname)
        self.layout.operator(SelectedEmptiesToVertices.ETO_tracked_vertices.bl_idname)

addon_keymaps = []

def menu_func(self, context):
    self.layout.separator()
    self.layout.menu(ETO_MT_menu.bl_idname)

def register():
    # Register the classes
    bpy.utils.register_class(SelectedEmptiesToPlane.ETO_tracked_plane)
    bpy.utils.register_class(SelectedEmptiesToVertices.ETO_tracked_vertices)
    bpy.utils.register_class(ETO_MT_menu)

    # Create the menu
    bpy.types.VIEW3D_MT_object.append(menu_func)

    # Add the keymap
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if wm.keyconfigs.addon:
        km = wm.keyconfigs.addon.keymaps.new(name="Object Mode", space_type="EMPTY")
        kmi = km.keymap_items.new(SelectedEmptiesToPlane.ETO_tracked_plane.bl_idname, 'P', 'PRESS', ctrl=True, shift=True)
        addon_keymaps.append((km, kmi))
        kmi = km.keymap_items.new(SelectedEmptiesToVertices.ETO_tracked_vertices.bl_idname, 'V', 'PRESS', ctrl=True, shift=True)
        addon_keymaps.append((km, kmi))

def unregister():
    # Remove the keymap
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    # Remove the menu
    bpy.types.VIEW3D_MT_object.remove(menu_func)

    # Unregister the classes
    bpy.utils.unregister_class(ETO_MT_menu)
    bpy.utils.unregister_class(SelectedEmptiesToPlane.ETO_tracked_plane)
    bpy.utils.unregister_class(SelectedEmptiesToVertices.ETO_tracked_vertices)

if __name__ == "__main__":
    register()
