import bpy
import bmesh

from . import (
    SelectionHelpers as sh,
    CreationHelpers as ch
)

class ETO_tracked_vertices(bpy.types.Operator):
    bl_idname = "eto.tracked_vertices"
    bl_description = "Create an object with vertices that are snapped to the selected empties."
    bl_label = "Create tracked vertices"

    def show_error(self, message, popup=True):
        self.report({"WARNING"}, message)
        if popup:
            bpy.context.window_manager.popup_menu(
                lambda self, context: self.layout.label(text=message),
                title="Error",
                icon="ERROR"
            )

    def execute(self, context):
        tracks = sh.get_selected_empties()

        if len(tracks) == 0:
            self.show_error("No objects have been selected that are recognized as trackers. Select a few first.", True)

        # Add an object to put the vertices in
        obj = ch.add_object()

        # Get all locations to put a vertex at
        locations = []
        for track in tracks:
            locations.append(sh.world_position(track))

        # Create the vertices
        ch.make_vertices(obj, locations)

        # Create all hooks
        ch.create_hooks(tracks, obj)

        bpy.context.window_manager.popup_menu(
            lambda self, context: self.layout.label(text="Vertices creation is complete"),
            title="Info",
            icon="INFO"
        )

        return {"FINISHED"}
