import bpy

def world_position(obj):
    # Return the world position of the object
    return obj.matrix_world.to_translation()

def get_selected_empties():
    # Return all selected objects that are of type "EMPTY"
    return [obj for obj in bpy.context.selected_objects if obj.type == "EMPTY"]

def grid_group(grid, confidence_x, confidence_y):
    # Group all objects in the list 'grid' into an group_x and group_y based
    # on their x and y values.
    grid_sy = sorted([world_position(obj).y for obj in grid])
    grid_sx = sorted([world_position(obj).x for obj in grid])

    diff_x = 0
    diff_y = 0
    for i in range(0, len(grid) - 1):
        diff_x += abs(grid_sx[i+1] - grid_sx[i])
        diff_y += abs(grid_sy[i+1] - grid_sy[i])
    avg_diff_x = diff_x / len(grid)
    avg_diff_y = diff_y / len(grid)

    # Group based on x difference
    groups_x = []
    for obj in grid:
        pos = world_position(obj)
        highest_confidence = (0, 0)
        for i in range(0, len(groups_x)):
            group_element = groups_x[i][0]
            dx = abs(world_position(group_element).x - pos.x)
            if dx != 0:
                confidence = avg_diff_x / dx
            else:
                confidence = 1
            if confidence > highest_confidence[1]:
                highest_confidence = (i, confidence)
        if highest_confidence[1] > confidence_x:
            groups_x[highest_confidence[0]].append(obj)
        else:
            groups_x.append([obj])

    # Group based on y difference
    groups_y = []
    for obj in grid:
        pos = world_position(obj)
        highest_confidence = (0, 0)
        for i in range(0, len(groups_y)):
            group_element = groups_y[i][0]
            dy = abs(world_position(group_element).y - pos.y)
            if dy != 0:
                confidence = avg_diff_y / dy
            else:
                confidence = 1
            if confidence > highest_confidence[1]:
                highest_confidence = (i, confidence)
        if highest_confidence[1] > confidence_y:
            groups_y[highest_confidence[0]].append(obj)
        else:
            groups_y.append([obj])

    return groups_x, groups_y

def checked_grid_group(grid, confidence_x, confidence_y):
    # Group the objects in 'grid' into an x group and y group, check if it's
    # a proper grid and reconstruct an ordered list of depth 1.
    groups_x, groups_y = grid_group(grid, confidence_x, confidence_y)

    # Check if it worked and reconstruct the grid in the right order
    x_worked = True
    y_worked = True
    sorted_grid = []
    groups_y.sort(key=lambda l: world_position(l[0]).y)
    for g in groups_x:
        if len(groups_y) != len(g):
            x_worked = False
    for g in groups_y:
        if len(groups_x) != len(g):
            y_worked = False
        g.sort(key=lambda entry: world_position(entry).x)
        sorted_grid.extend(g)

    return x_worked, y_worked, sorted_grid, groups_x, groups_y

def is_equal_lengths_double_list(l):
    length = None
    for e in l:
        if length == None:
            length = len(e)
        else:
            if len(e) != length:
                return False
    return True

def auto_confidence_grid_group(grid, stepsize=0.01):
    # This attempts to find the highest confidence for which the groups can be
    # created
    # It does so by lowering the confidence (by 'stepsize') until it finds
    # valid groups
    confidence_x = 1.0
    confidence_y = 1.0

    x_worked, y_worked, sorted_grid, groups_x, groups_y = checked_grid_group(grid, confidence_x, confidence_y)

    reset_x = False
    reset_y = False

    while not (x_worked and y_worked):
        changed_x = False
        changed_y = False
        if not is_equal_lengths_double_list(groups_x):
            confidence_x -= stepsize
            changed_x = True
        if not is_equal_lengths_double_list(groups_y):
            confidence_y -= stepsize
            changed_y = True
        if not (changed_x or changed_y):
            if confidence_x == confidence_y == 1:
                # Both are stuck on confidence 1
                # This only seems to happen when your tracks are on exactly
                # the same x and y values...
                confidence_x -= stepsize
                confidence_y -= stepsize
            # If both are equal lengths, reset the lowest confidence and try again
            # This may work because the groups are either allowed or not based
            # on each other. The lower one is reset, because a lower confidence
            # means a lower quality
            elif confidence_x < confidence_y and not reset_x:
                confidence_x = 1
                reset_x = True
            elif not reset_y:
                confidence_y = 1
                reset_y = True
            elif not reset_x:
                # Bit of an edge case this...
                confidence_x = 1
                reset_x = True
            else:
                break

        if confidence_x < 0 or confidence_y < 0:
            return -1, -1, -1, confidence_x, confidence_y

        x_worked, y_worked, sorted_grid, groups_x, groups_y = checked_grid_group(grid, confidence_x, confidence_y)

    columns = len(groups_x)
    rows = len(groups_y)
    return sorted_grid, columns, rows, confidence_x, confidence_y
