import bpy
import bmesh

from . import SelectionHelpers as sh

def hook_vertex_to_object(vertex, obj, use_bone=False):
    # Hooks a vertex to an object
    vertex.select_set(True)
    obj.select_set(True)
    bpy.ops.object.hook_add_selob(use_bone=use_bone)
    obj.select_set(False)

def get_bmesh(obj):
    # Gets the mesh from an edited object
    mesh = bmesh.from_edit_mesh(obj.data)
    mesh.verts.ensure_lookup_table()
    return mesh

def get_vertices(plane_object):
    # Returning the plane makes sure it doesn't get removed yet
    plane = get_bmesh(plane_object)
    vertices = [v for v in plane.verts]
    vertices.sort(key=lambda v: (v.co.y, v.co.x))
    return plane, vertices

def add_plane():
    bpy.ops.mesh.primitive_plane_add(enter_editmode=True, location=(0, 0, 0))
    return bpy.context.selected_objects[0]

def add_object():
    plane = add_plane()
    mesh = get_bmesh(plane)
    mesh.clear()
    return plane

def make_vertices(obj, locations):
    bpy.ops.object.mode_set(mode='OBJECT')
    mesh = obj.data
    mesh.from_pydata(locations, [], [])
    mesh.calc_normals()
    mesh.update()
    bpy.ops.object.mode_set(mode='EDIT')

def add_loopcuts(columns, rows):
    # This makes loopcuts in the currently selected object, which must be in edit mode
    edge_slide = {
        "value":-0,
        "single_side":False,
        "use_even":False,
        "flipped":False,
        "use_clamp":True,
        "mirror":True,
        "snap":False,
        "snap_target":'CLOSEST',
        "snap_point":(0, 0, 0),
        "snap_align":False,
        "snap_normal":(0, 0, 0),
        "correct_uv":True,
        "release_confirm":False,
        "use_accurate":False
    }
    if columns > 0:
        # Attempting to add zero loopcuts will still make a loopcut
        bpy.ops.mesh.loopcut_slide(
            MESH_OT_loopcut = {
                "number_cuts": columns,
                "smoothness": 0,
                "falloff": 'INVERSE_SQUARE',
                "object_index": 0,
                "edge_index": 1,
                "mesh_select_mode_init": (True, False, False)
            },
            TRANSFORM_OT_edge_slide = edge_slide
        )
    if rows > 0:
        # Attempting to add zero loopcuts will still make a loopcut
        bpy.ops.mesh.loopcut_slide(
            MESH_OT_loopcut = {
                "number_cuts": rows,
                "smoothness": 0,
                "falloff": 'INVERSE_SQUARE',
                "object_index": 0,
                "edge_index": 0,
                "mesh_select_mode_init": (True, False, False)
            },
            TRANSFORM_OT_edge_slide = edge_slide
        )

def set_vertices_locations(tracks, vertices):
    # The vertex on index 'x' in vertices is moved to the location
    # of the track on index 'x' in tracks
    # So they must be ordered in the same way!
    for vertex, track in zip(vertices, tracks):
        vertex.co = sh.world_position(track)

def create_hooks(empties, obj):
    # This function hooks a vertex to every track
    # The vertex MUST be on the same location as the track
    for empty in empties:
        bpy.ops.mesh.select_all(action='DESELECT')
        mesh = get_bmesh(obj)
        vertex = None
        for v in mesh.verts:
            # Find the right vertex for the tracker, based on location
            if v.co == sh.world_position(empty):
                vertex = v
                break
        if vertex == None:
            # TODO: Make this a popup/notification or something
            print("Track found without vertex. This should not happen.")
            print("Process will continue. Manual adjustment will be necessary.")
            print()
        else:
            hook_vertex_to_object(vertex, empty)
