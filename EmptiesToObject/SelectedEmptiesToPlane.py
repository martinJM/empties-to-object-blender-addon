import bpy
import bmesh

from . import (
    SelectionHelpers as sh,
    CreationHelpers as ch
)

class ETO_tracked_plane(bpy.types.Operator):
    bl_idname = "eto.tracked_plane"
    bl_description = "Create a plane that is snapped to the selected empties (which must be in a grid)."
    bl_label = "Create tracked plane"
    # bl_options = {"REGISTER", "UNDO"}

    auto_confidence: bpy.props.BoolProperty(
        name="Auto confidence",
        default=True
    )
    confidence_x: bpy.props.FloatProperty(
        name="Confidence on x axis",
        default=0.5,
        min=0,
        max=1
    )
    confidence_y: bpy.props.FloatProperty(
        name="Confidence on y axis",
        default=0.5,
        min=0,
        max=1
    )

    def show_error(self, message, popup=True):
        self.report({"WARNING"}, message)
        if popup:
            bpy.context.window_manager.popup_menu(
                lambda self, context: self.layout.label(text=message),
                title="Error",
                icon="ERROR"
            )

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="Create a tracked plane")

        row = col.row()
        row.prop(self, "auto_confidence")

        row = col.row()
        row.enabled = not self.auto_confidence
        row.prop(self, "confidence_y", slider=True)
        row.prop(self, "confidence_x", slider=True)
        row = col.row()

    def execute(self, context):
        tracks = sh.get_selected_empties()

        if len(tracks) == 0:
            self.show_error("No objects have been selected that are recognized as trackers. Select a few first.", True)
            return {"CANCELLED"}
        if len(tracks) < 4:
            self.show_error("Select at least 4 trackers to make a plane.")
            return {"CANCELLED"}

        if self.auto_confidence == True:
            sorted_tracks, columns, rows, self.confidence_x, self.confidence_y = sh.auto_confidence_grid_group(tracks)

            if sorted_tracks == -1:
                self.show_error("The tracks are not aligned to a grid enough.\nLowest confidences that were attempted (x, y): ({}, {})".format(
                    self.confidence_x, self.confidence_y
                ), true)
                return {"CANCELLED"}
        else:
            x_worked, y_worked, sorted_tracks, groups_x, groups_y = sh.checked_grid_group(tracks, self.confidence_x, self.confidence_y)

            if not (x_worked and y_worked):
                self.show_error("Could not detect a proper grid with these confidences.", True)
                return {"CANCELLED"}

            columns = len(groups_x)
            rows = len(groups_y)

        # Make the plane
        plane_object = ch.add_plane()

        # Set edit mode
        bpy.ops.object.mode_set(mode='EDIT')

        # Add the necessary loopcuts
        ch.add_loopcuts(columns-2, rows-2)

        # Get all vertices
        _, vertices = ch.get_vertices(plane_object)

        # Check that the amount of vertices equals the amount of tracks
        if len(vertices) != len(sorted_tracks):
            self.show_error("Vertex and track count doesn't match: {}, {}.\nCannot make a proper plane.".format(
                len(vertices), len(sorted_tracks)
            ), True)
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.data.objects.remove(plane_object)
            return {"FINISHED"}

        # Set the locations of the vertices to the locations of the tracks
        ch.set_vertices_locations(sorted_tracks, vertices)

        # Create all hooks
        ch.create_hooks(sorted_tracks, plane_object)

        # Get out of edit mode
        bpy.ops.object.mode_set(mode='OBJECT')

        popup_data = (self.confidence_x, self.confidence_y)
        bpy.context.window_manager.popup_menu(
            lambda self, context: self.layout.label(text="Plane created with confidences (x, y): {}".format(
                popup_data
            )),
            title="Info",
            icon="INFO"
        )

        return {"FINISHED"}
