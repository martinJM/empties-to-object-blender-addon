# Empties to Object

This addon can be used to create an object from empties in Blender 2.80.

It started as taking the challenge of scripting the manual labour in a Youtube video [1].

[1] https://www.youtube.com/watch?v=aAiwLkEulTs

## What can it do?

Create a plane that is hooked to all empties. For this to work, the empties need to be in a grid. Some variation in the grid is not an issue, but if you get strange (or no) results, you may want to try and perform the operation on a better grid.

For a better idea of this function: it can do the parts between 15:00 and 21:40 of the previously mentioned video [1].

It can also make an object that has vertices on the locations of the empties and are hooked to them. This function will not create any edges or faces. To create a nice object from this, you'll have to add the edges and faces yourself, so it's a bit more work. However, this function is more robust than the Create Tracked Plane function, so it may be preferred when that function fails.

[1] https://www.youtube.com/watch?v=aAiwLkEulTs


## How to install

There are three download options:

1. You can download the latest release I made by going to the releases page: https://gitlab.com/martinJM/empties-to-object-blender-addon/-/releases

2. You can clone/download this repository, and zip the `EmptiesToObject` directory.

3. You can use the `Makefile` if you have both `zip` and `make`
installed (mostly for Linux/Mac).

The zip that you get from either of these steps can be installed in Blender through the Add-ons preferences section.
Click the install button, select the zip file, and press "Install Add-on from File...".
After this, you can enable it by searching for "Empties to Object" and check the box to the left of the name (still in the Add-ons preferences section).

## How can I use it?

Install it as described in "How to install". In the 3D view, select all the empty objects you want to convert in a tracked plane. It doesn't matter if you also select some objects that are not empty, as the add-on only uses the selected empty objects.

Click on the "Object" menu item. There should now be an entry named "Empties to Object" with a small triangle next to it. Select that entry, which should open a new menu, containing the entry "Create tracked plane". Click this entry to create a tracked plane. It also contains the entry "Create tracked vertices". Click this entry to create tracked vertices.


## "It doesn't work"

Messages from this add-on:

| Message | Solution |
| ------- | ----- |
| No objects have been selected | Select multiple empties |
| Track found without vertex | Do some manual adjustment on the generated plane, or try to align the empties better to a grid (or try it on a different frame) |
| The tracks are not aligned to a grid enough | Align the tracks better to a grid (for a camera track, go to a different frame and try again) |
| Could not detect a proper grid with these confidences | Try lower confidences or try auto confidence (recommended) |
| Vertex and track count doesn't match | Try again with the empties aligned better (or with a different frame). |
